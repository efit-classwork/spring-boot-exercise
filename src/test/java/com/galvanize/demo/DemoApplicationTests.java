package com.galvanize.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void returnNumberShouldReturnOne(){
		ReturnController testController = new ReturnController();
		int actual = testController.returnNumber();
		int expected = 1;
		assertEquals(actual,expected);
	}

}
